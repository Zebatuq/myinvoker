import MyGameState from "./MyGameState";

abstract class MySkillsController {

    public static Quas: Ability;
    public static Wex: Ability;
    public static Exort: Ability;

    public static ColdSnap: Ability;
    public static GhostWalk: Ability;
    public static Tornado: Ability;
    public static Emp: Ability;
    public static Alacrity: Ability;
    public static ChaosMeteor: Ability;
    public static SunStrike: Ability;
    public static ForgeSpirit: Ability;
    public static IceWall: Ability;
    public static DeafeningBlast: Ability;

    public static Init(): void{
        MySkillsController.Quas = MyGameState.MyHero.GetAbilityByIndex(0);
        MySkillsController.Wex = MyGameState.MyHero.GetAbilityByIndex(1);
        MySkillsController.Exort = MyGameState.MyHero.GetAbilityByIndex(2);

        MySkillsController.ColdSnap = MyGameState.MyHero.GetAbility("invoker_cold_snap");
        MySkillsController.GhostWalk = MyGameState.MyHero.GetAbility("invoker_ghost_walk");
        MySkillsController.Tornado = MyGameState.MyHero.GetAbility("invoker_tornado");
        MySkillsController.Emp = MyGameState.MyHero.GetAbility("invoker_emp");
        MySkillsController.Alacrity = MyGameState.MyHero.GetAbility("invoker_alacrity");
        MySkillsController.ChaosMeteor = MyGameState.MyHero.GetAbility("invoker_chaos_meteor");
        MySkillsController.SunStrike = MyGameState.MyHero.GetAbility("invoker_sun_strike");
        MySkillsController.ForgeSpirit = MyGameState.MyHero.GetAbility("invoker_forge_spirit");
        MySkillsController.IceWall = MyGameState.MyHero.GetAbility("invoker_ice_wall");
        MySkillsController.DeafeningBlast = MyGameState.MyHero.GetAbility("invoker_deafening_blast");
    }

    public static SetSphere(sphere1: Ability, sphere2: Ability, sphere3: Ability){

        let myHero = MyGameState.MyHero;

        if(sphere1.GetLevel() > 0){
            sphere1.CastNoTarget();
        }

        if(sphere2.GetLevel() > 0){
            sphere1.CastNoTarget();
        }

        if(sphere3.GetLevel() > 0){
            sphere1.CastNoTarget();
        }

    }
}

export default MySkillsController;