import MySkillController from "./MySkillController";

abstract class MyGameController{

    public static IsActiveGame: boolean = false;
    public static MyPlayer: Player;
    public static MyHero: Hero;
    public static MyHeroName = 'npc_dota_hero_invoker';

    public static Init(): void{

        if (!GameRules.IsActiveGame()) {
            return;
        }

        MyGameController.MyHero = EntitySystem.GetLocalHero();

        if (!MyGameController.MyHero){
            return;
        }

        if (!MyGameController.MyHero.IsExist()){
            return;
        }

        if (MyGameController.MyHero.GetUnitName() !== MyGameController.MyHeroName){
            return;
        }

        MyGameController.MyPlayer = EntitySystem.GetLocalPlayer();


        MySkillController.Init();

        MyGameController.IsActiveGame = true;

    }

}

export default MyGameController;