import MyGameController from "./MyGameController";
import {SphereType} from "./Enums";

export abstract class MySkillController{

    public static Spheres: Array<Ability>;
    public static Skills: Array<Ability>;

    public static Init(): void {

        MySkillController.Spheres = [
            MyGameController.MyHero.GetAbilityByIndex(0),
            MyGameController.MyHero.GetAbilityByIndex(1),
            MyGameController.MyHero.GetAbilityByIndex(2)
        ];

        MySkillController.Skills = [
            MyGameController.MyHero.GetAbility("invoker_cold_snap"),
            MyGameController.MyHero.GetAbility("invoker_ghost_walk"),
            MyGameController.MyHero.GetAbility("invoker_tornado"),
            MyGameController.MyHero.GetAbility("invoker_emp"),
            MyGameController.MyHero.GetAbility("invoker_alacrity"),
            MyGameController.MyHero.GetAbility("invoker_chaos_meteor"),
            MyGameController.MyHero.GetAbility("invoker_sun_strike"),
            MyGameController.MyHero.GetAbility("invoker_forge_spirit"),
            MyGameController.MyHero.GetAbility("invoker_ice_wall"),
            MyGameController.MyHero.GetAbility("invoker_deafening_blast")
        ];

    }

    public static SetSphere(sphere1: SphereType, sphere2: SphereType, sphere3: SphereType){

        let myHero = MyGameController.MyHero;

        if(MySkillController.Spheres[sphere1].GetLevel() > 0){
            MyGameController.MyPlayer.PrepareUnitOrders(
                Enum.UnitOrder.DOTA_UNIT_ORDER_CAST_NO_TARGET,
                myHero,
                null,
                MySkillController.Spheres[sphere1],
                3, myHero);
        }

        if(MySkillController.Spheres[sphere2].GetLevel() > 0){
            MyGameController.MyPlayer.PrepareUnitOrders(
                Enum.UnitOrder.DOTA_UNIT_ORDER_CAST_NO_TARGET,
                myHero,
                null,
                MySkillController.Spheres[sphere2],
                3, myHero);
        }

        if(MySkillController.Spheres[sphere3].GetLevel() > 0){
            MyGameController.MyPlayer.PrepareUnitOrders(
                Enum.UnitOrder.DOTA_UNIT_ORDER_CAST_NO_TARGET,
                myHero,
                null,
                MySkillController.Spheres[sphere3],
                3, myHero);
        }

    }
}

export default MySkillController;