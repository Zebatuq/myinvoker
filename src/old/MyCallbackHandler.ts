import MyMenu from "./MyMenu";
import MyGameController from "./MyGameController";
import MyUI from "./MyUI";
import {MySkillController} from "./MySkillController";
import {SphereType} from "./Enums";

namespace CallBack{


    export abstract class MyCallbackHandler{

        public static OnInit(): void{

            MyMenu.Global.Init();
            MyGameController.Init();
        }

        public static OnUpdate(): void{

            if(!MyGameController.IsActiveGame){
                return;
            }

            MyUI.Global.Calculate();

        }

        public static OnDraw(): void{

            if(!MyGameController.IsActiveGame){
                return;
            }

            MyUI.Global.Draw();

        }

        public static OnPrepareUnitOrders(order: PreparedOrder): void{

            if(!MyGameController.IsActiveGame){
                return;
            }

            if(order.order == Enum.UnitOrder.DOTA_UNIT_ORDER_ATTACK_TARGET){

                if(MyMenu.AutoSphere.Exort){
                    MySkillController.SetSphere(SphereType.Exort, SphereType.Exort, SphereType.Exort);
                }

            }

            if(order.order == Enum.UnitOrder.DOTA_UNIT_ORDER_MOVE_TO_POSITION){

                if(MyMenu.AutoSphere.Wex){
                    MySkillController.SetSphere(SphereType.Wex, SphereType.Wex, SphereType.Wex);
                }

            }

            if(order.order == Enum.UnitOrder.DOTA_UNIT_ORDER_HOLD_POSITION){

                if(MyMenu.AutoSphere.Quas){
                    MySkillController.SetSphere(SphereType.Quas, SphereType.Quas, SphereType.Quas);
                }

            }

        }

        public static OnKeyEvent(keyEvent: KeyEventObject): void{

            if (keyEvent.key === Enum.ButtonCode.MOUSE_FIRST) {

                if(MyMenu.SkillPanel.Dragable){
                    if(Input.IsCursorInRect(MyMenu.SkillPanel.X - 20, MyMenu.SkillPanel.Y - 20, 20, 20)) {
                        MyUI.SkillPanel.MouseDownOnSkillPanelMovabler = true;
                    }
                }

            }

        }

    }

}


export default CallBack;