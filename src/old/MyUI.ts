import MySkillController from "./MySkillController";
import MyMenu from "./MyMenu";

namespace MyUI{

    export abstract class Global{

        public static Calculate(){

            if(MyMenu.SkillPanel.Enabled){
                MyUI.SkillPanel.Calculate();
            }

        }

        public static Draw(){

            if(MyMenu.SkillPanel.Enabled){
                MyUI.SkillPanel.Draw();
            }

        }

    }

    export abstract class SkillPanel{

        public static SkillPanel: SkillPanelModel;
        public static MouseDownOnSkillPanelMovabler: boolean = false;

        public static Draw(): void{

            let skillPanel = SkillPanel.SkillPanel;

            if(!skillPanel){
                return;
            }

            let currentPosX = skillPanel.X;
            let offsetToAdd = skillPanel.ImageSize;

            if(skillPanel.Dragable){
                Renderer.SetDrawColor(255, 255, 255, skillPanel.ImageOpacity);
                Renderer.DrawFilledRect(skillPanel.X - 20, skillPanel.Y - 20, 20, 20);
            }

            skillPanel.Skills.forEach(skill =>{

                Renderer.SetDrawColor(255, 255, 255, skillPanel.ImageOpacity);
                Renderer.DrawImage(skill.Image, currentPosX, skillPanel.Y, skillPanel.ImageSize, skillPanel.ImageSize);

                if(skillPanel.DrawCooldownNumber){

                    if(skill.NeedDrawCooldown){
                        let textX = currentPosX + offsetToAdd / 2;
                        let textY = skillPanel.Y + offsetToAdd / 2;

                        Renderer.SetDrawColor(0, 0, 0, 100);
                        Renderer.DrawFilledRect(currentPosX, skillPanel.Y, skillPanel.ImageSize, skillPanel.ImageSize);

                        Renderer.SetDrawColor(255, 255, 255, 255);
                        Renderer.DrawTextCentered(skillPanel.CooldownNumberFont, textX, textY, skill.CoolDown);
                    }

                }

                currentPosX += offsetToAdd;

            });

        }

        public static Calculate(): void {

            let newSkillPanel = new SkillPanelModel();

            newSkillPanel.X = MyMenu.SkillPanel.X;
            newSkillPanel.Y = MyMenu.SkillPanel.Y;
            newSkillPanel.ImageSize = MyMenu.SkillPanel.Size;
            newSkillPanel.ImageOpacity = MyMenu.SkillPanel.Opacity;
            newSkillPanel.DrawCooldownNumber = MyMenu.SkillPanel.DrawCooldownNumber;
            newSkillPanel.CooldownNumberFont = MyMenu.SkillPanel.CooldownNumberFont;
            newSkillPanel.Dragable = MyMenu.SkillPanel.Dragable;

            let newSkills: Array<SkillModel> = [];

            let currentPosX = newSkillPanel.X;
            let currentPosY = newSkillPanel.Y;
            let offsetToAdd = newSkillPanel.ImageSize;

            MySkillController.Skills .forEach(skill => {

                let newSkill: SkillModel = new SkillModel();

                newSkill.X = currentPosX;
                newSkill.Y = currentPosY;
                newSkill.Image = skill.GetImage();

                if(newSkillPanel.DrawCooldownNumber){
                    newSkill.CoolDown = skill.GetCooldown().toFixed(1).toString();

                    if(newSkill.CoolDown != "0.0"){
                        newSkill.NeedDrawCooldown = true;
                    }
                }

                newSkills.push(newSkill);

                currentPosX += offsetToAdd;
            });

            newSkillPanel.Skills = newSkills;

            if(newSkillPanel.Dragable){

                if(SkillPanel.MouseDownOnSkillPanelMovabler){
                    if(Input.IsKeyDown(Enum.ButtonCode.MOUSE_FIRST)) {

                        let [mouseX, mouseY] = Input.GetCursorPos();

                        newSkillPanel.X = mouseX + 10;
                        newSkillPanel.Y = mouseY + 10;

                        MyMenu.SkillPanel.X = newSkillPanel.X;
                        MyMenu.SkillPanel.Y =  newSkillPanel.Y;

                        Config.WriteInt('MyInvoker', 'SkillPanelX', newSkillPanel.X);
                        Config.WriteInt('MyInvoker', 'SkillPanelY', newSkillPanel.Y);


                    }else{
                        SkillPanel.MouseDownOnSkillPanelMovabler = false;
                    }
                }

            }

            SkillPanel.SkillPanel = newSkillPanel;
        }

    }

    class SkillPanelModel {

        public X: number;
        public Y: number;
        public ImageSize: number;
        public ImageOpacity: number;
        public Dragable: boolean;

        public DrawCooldownNumber: boolean;
        public CooldownNumberFont: Font;

        public Skills: Array<SkillModel> = [];

    }

    class SkillModel{
        public Image: LoadedImage;
        public NeedDrawCooldown: boolean = false;
        public CoolDown: string;
        public X: number;
        public Y: number;
        public IsMouseHover: boolean;
    }

}

export default MyUI;