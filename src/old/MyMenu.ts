namespace MyMenu{

    export abstract class Global{

        public static BaseMenuPath =  ['My', 'Invoker'];

        public static Init(): void{
            Global.SetImages();
        }

        public static SetImages(): void{
            Menu.SetImage(Global.BaseMenuPath, 'panorama/images/heroes/icons/npc_dota_hero_invoker_png.vtex_c');
            Menu.SetImage(AutoSphere.MenuPath, 'panorama/images/spellicons/invoker_exort_png.vtex_c');
        }

    }

    export abstract class AutoSphere {

        public static MenuPath = Global.BaseMenuPath.concat('Авто сферы');

        public static Quas = Menu.AddToggle(AutoSphere.MenuPath, 'Quas при остановке', false)
            .OnChange(state => (AutoSphere.Quas = state.newValue))
            .GetValue();

        public static Wex = Menu.AddToggle(AutoSphere.MenuPath, 'Wex при ходьбе', false)
            .OnChange(state => (AutoSphere.Wex = state.newValue))
            .GetValue();

        public static Exort = Menu.AddToggle(AutoSphere.MenuPath, 'Exort при атаке', false)
            .OnChange(state => (AutoSphere.Exort = state.newValue))
            .GetValue();

    }

    export abstract class SkillPanel {

        public static MenuPath = Global.BaseMenuPath.concat('Панель скиллов');

        public static Enabled = Menu.AddToggle(SkillPanel.MenuPath, 'Панель скиллов', false)
            .OnChange(state => SkillPanel.Enabled = state.newValue)
            .GetValue();

        public static DrawCooldownNumber = Menu.AddToggle(SkillPanel.MenuPath, 'Цифровой кулдаун', false)
            .OnChange(state => SkillPanel.DrawCooldownNumber = state.newValue)
            .GetValue();

        public static DrawCooldownAnimation = Menu.AddToggle(SkillPanel.MenuPath, 'Анимационный кулдаун', false)
            .OnChange(state => SkillPanel.DrawCooldownAnimation = state.newValue)
            .GetValue();

        public static Size = Menu.AddSlider(SkillPanel.MenuPath, 'Размер панели', 10, 150, 10, 1)
            .OnChange(state => {
                SkillPanel.Size = state.newValue;
                SkillPanel.CooldownNumberFont = Renderer.LoadFont('Arial', SkillPanel.Size / 2, Enum.FontWeight.NORMAL);
            })
            .GetValue();

        public static Opacity = Menu.AddSlider(SkillPanel.MenuPath, 'Прозрачность панели', 0, 255, 155, 1)
            .OnChange(state => (SkillPanel.Opacity = state.newValue))
            .GetValue();

        public static OpacityHover = Menu.AddSlider(SkillPanel.MenuPath, 'Прозрачность при наведении', 0, 255, 255, 1)
            .OnChange(state => (SkillPanel.OpacityHover = state.newValue))
            .GetValue();

        //public static X = Menu.AddSlider(SkillPanel.MenuPath, 'X', 10, 150, 10, 1)
        //    .OnChange(state => (SkillPanel.X = state.newValue))
        //    .GetValue();

        public static Dragable = Menu.AddToggle(SkillPanel.MenuPath, 'Передвигаемая панель', false)
            .OnChange(state => (SkillPanel.Dragable = state.newValue))
            .GetValue();

        public static Castable = Menu.AddToggle(SkillPanel.MenuPath, 'Клик для каста', false)
            .OnChange(state => (SkillPanel.Castable = state.newValue))
            .GetValue();


        public static CooldownNumberFont = Renderer.LoadFont('Arial', SkillPanel.Size / 2, Enum.FontWeight.NORMAL);

        public static X = Config.ReadInt('MyInvoker', 'SkillPanelX', 200);

        public static Y = Config.ReadInt('MyInvoker', 'SkillPanelY', 200);

    }

}

export default MyMenu;