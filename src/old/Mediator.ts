class EventStorage {
    private static classInstance : EventStorage;
    private events: { [id: string]: Array<(data : Object) => void>; } = {};

    private constructor() {}

    public get<T>(eventName : string) : Array<(data: T) => void> {
        if (!this.events[eventName]) {
            return [];
        }

        return this.events[eventName];
    }

    public add<T>(eventName : number, callback: (data : T) => void) {
        if (!this.events[eventName]) {
            this.events[eventName] = [];
        }
        this.events[eventName].push(callback);
    }

    public static instance() : EventStorage {
        if (EventStorage.classInstance == null) {
            EventStorage.classInstance = new EventStorage();
        }
        return EventStorage.classInstance;
    }
}

class Mediator implements IMediator {
    public constructor() { }

    public publish<T>(event: IEvent<T>): IMediator {
        let subscriptions = EventStorage.instance().get(event.type.toString());

        for (var i = 0, l = subscriptions.length; i < l; i++) {
            let subscription = subscriptions[i];
            subscription(event.data);
        }
        return this;
    };

    public subscribe<T>(type: number, callback: (data : T) => void) : IMediator {
        EventStorage.instance().add(type, callback);
        return this;
    };
}

interface IMediator {
    publish<T>(event: IEvent<T>) : IMediator;
    subscribe<T>(type: number, callback: (data : T) => void)  : IMediator;
}

interface IEvent<T> {
    type: number;
    data: T;
}

export { IEvent, IMediator, Mediator }