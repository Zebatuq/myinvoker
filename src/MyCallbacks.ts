import Action from "./Tools/Action";

abstract class MyCallbacks {

    public static OnUpdate: Action = new Action();
    public static OnDraw: Action = new Action();
    public static OnScriptLoad: Action = new Action();
    public static OnGameStart: Action = new Action();
    public static OnPrepareUnitOrders: Action<PreparedOrder> = new Action<PreparedOrder>();
    public static OnKeyEvent: Action<KeyEventObject> = new Action<KeyEventObject>();

}

export default MyCallbacks;