import MyInvoker from "../../MyInvoker";
import MyMainMenu from "../../MyMainMenu";
import IMenu from "../../Interfaces/IMenu";
import MySkillsController from "../../MySkillsController";

class AutoSpheresMenu implements IMenu{

    public MenuPath: string[] = MyMainMenu.MenuPath.concat("Auto Spheres");

    public Quas = Menu.AddToggle(this.MenuPath, "Quas при остановке", false)
        .SetNameLocale("ru", "Quas при остановке")
        .SetNameLocale("en", "Quas when stop")
        .OnChange(state => (this.Quas = state.newValue))
        .GetValue();

    public Wex = Menu.AddToggle(this.MenuPath, "Wex при ходьбе", false)
        .SetNameLocale("ru", "Wex при ходьбе")
        .SetNameLocale("en", "Wex when walk")
        .OnChange(state => (this.Wex = state.newValue))
        .GetValue();

    public Exort = Menu.AddToggle(this.MenuPath, "Exort при атаке", false)
        .SetNameLocale("ru", "Exort при атаке")
        .SetNameLocale("en", "Exort when attack")
        .OnChange(state => (this.Exort = state.newValue))
        .GetValue();

}
export default AutoSpheresMenu;