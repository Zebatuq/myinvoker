import IScript from "../../Interfaces/IScript";
import AutoSpheresMenu from "./AutoSpheresMenu";
import MyCallbacks from "../../MyCallbacks";
import MySkillsController from "../../MySkillsController";

class AutoSpheres implements IScript{

    public Menu: AutoSpheresMenu = new AutoSpheresMenu();

    public Init(): void{

       MyCallbacks.OnPrepareUnitOrders.Subscribe((order) => {
           this.OnPrepareUnitOrders(order);
       });

    }

    private OnPrepareUnitOrders(order: PreparedOrder): void{

        if(order.order == Enum.UnitOrder.DOTA_UNIT_ORDER_ATTACK_TARGET){
            if(this.Menu.Exort){
                MySkillsController.SetSphere(MySkillsController.Exort, MySkillsController.Exort, MySkillsController.Exort);
            }
        }

        if(order.order == Enum.UnitOrder.DOTA_UNIT_ORDER_MOVE_TO_POSITION){
            if(this.Menu.Wex){
                MySkillsController.SetSphere(MySkillsController.Wex, MySkillsController.Wex, MySkillsController.Wex);
            }
        }

        if(order.order == Enum.UnitOrder.DOTA_UNIT_ORDER_HOLD_POSITION){
            if(this.Menu.Quas){
                MySkillsController.SetSphere(MySkillsController.Quas, MySkillsController.Quas, MySkillsController.Quas);
            }
        }
    }

}

export default AutoSpheres;