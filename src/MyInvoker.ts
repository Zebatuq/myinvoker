import "./MyMainMenu";
import MyMainMenu from "./MyMainMenu";
import MyCallbacks from "./MyCallbacks";
import MyGameState from "./MyGameState";
import IScript from "./Interfaces/IScript";
import AutoSpheres from "./Scripts/AutoSpheres/AutoSpheres";
import SkillPanel from "./Scripts/SkillPanel/SkillPanel";
import MySkillsController from "./MySkillsController";

abstract class MyInvoker{

    public static Test: string = "123";

    public static Script: ScriptDescription = {};
    public static Scripts: Array<IScript> = [];

    public static Init = (() => {

        MyInvoker.AddScript( new AutoSpheres() );
        MyInvoker.AddScript( new SkillPanel() );

        MyInvoker.InitScripts();

        MyInvoker.Subscribe();

        RegisterScript(MyInvoker.Script);
    })();

    private static AddScript(script: IScript): void{
        MyInvoker.Scripts.push(script);
    }

    private static InitScripts(): void{
        MyInvoker.Scripts.forEach(script =>{
            script.Init();
        });
    }

    private static Subscribe(): void{

        MyInvoker.Script.OnGameStart = () => {
            MyCallbacks.OnGameStart.Invoke();
        };

        MyInvoker.Script.OnScriptLoad = () => {
            MyCallbacks.OnScriptLoad.Invoke();
        };

        MyInvoker.Script.OnUpdate = () => {
            if(MyMainMenu.Enabled && MyGameState.IsGameActive){
                MyCallbacks.OnUpdate.Invoke();
            }
        };

        MyInvoker.Script.OnDraw = () => {
            if(MyMainMenu.Enabled && MyGameState.IsGameActive){
                MyCallbacks.OnDraw.Invoke();
            }
        };

        MyInvoker.Script.OnPrepareUnitOrders = (order) => {
            if(MyMainMenu.Enabled && MyGameState.IsGameActive)
            {
                MyCallbacks.OnPrepareUnitOrders.Invoke(order);
            }
        };

        MyInvoker.Script.OnKeyEvent = (keyEvent) => {
            if(MyMainMenu.Enabled && MyGameState.IsGameActive)
            {
                MyCallbacks.OnKeyEvent.Invoke(keyEvent);
            }
        };

    }
}

export default MyInvoker;