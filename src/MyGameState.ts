import AutoSpheres from "./Scripts/AutoSpheres/AutoSpheres";
import MyCallbacks from "./MyCallbacks";
import MySkillsController from "./MySkillsController";

abstract class MyGameState {

    public static IsGameActive: boolean = false;
    public static MyPlayer: Player;
    public static MyHero: Hero;
    public static MyHeroName = 'npc_dota_hero_invoker';

    public static Init = (() => {

        MyCallbacks.OnScriptLoad.Subscribe(MyGameState.OnStart);
        MyCallbacks.OnGameStart.Subscribe(MyGameState.OnStart);

    })();

    public static OnStart(): void{
        if (!GameRules.IsActiveGame()) {
            return;
        }

        MyGameState.MyHero = EntitySystem.GetLocalHero();

        if (!MyGameState.MyHero){
            return;
        }

        if (!MyGameState.MyHero.IsExist()){
            return;
        }

        if (MyGameState.MyHero.GetUnitName() !== MyGameState.MyHeroName){
            return;
        }

        MyGameState.MyPlayer = EntitySystem.GetLocalPlayer();
        MySkillsController.Init();
        MyGameState.IsGameActive = true;
    }

}

export default MyGameState;