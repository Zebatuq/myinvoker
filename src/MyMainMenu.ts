import AutoSpheres from "./Scripts/AutoSpheres/AutoSpheres";
import IMenu from "./Interfaces/IMenu";

abstract class MyMainMenu implements IMenu{

    public static MenuPath: string[] = ['My', 'Invoker'];
    public static MenuImage: string = 'panorama/images/heroes/icons/npc_dota_hero_invoker_png.vtex_c';

    public static Enabled = Menu.AddToggle(MyMainMenu.MenuPath, 'Enable', false)
        .SetNameLocale("ru","Включен")
        .SetNameLocale("en","Enable")
        .OnChange(state => (MyMainMenu.Enabled = state.newValue))
        .GetValue();

    public static Init = (() => {

        Menu.SetImage(MyMainMenu.MenuPath, MyMainMenu.MenuImage);

    })();
}


export default MyMainMenu;