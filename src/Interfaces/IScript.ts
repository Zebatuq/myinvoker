import IMenu from "./IMenu";

abstract class IScript {

    public abstract Menu: IMenu;

    public abstract Init(): void;

}

export default IScript;