export enum EventType {
    OnInit
}

export enum SphereType {
    Quas,
    Wex,
    Exort
}

export enum SkillType {
    ColdSnap,
    GhostWalk,
    Tornado,
    Emp,
    Alacrity,
    ChaosMeteor,
    SunStrike,
    ForgeSpirit,
    IceWall,
    DeafeningBlast
}