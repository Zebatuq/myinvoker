class Action<T = undefined>{

    private _subscribers: Array<(arg:T) => void> = new Array<(arg:T) => void>();

    public Invoke(arg:T = undefined): void{
        this._subscribers.forEach(subscriber => {
            subscriber(arg);
        });
    }

    public Subscribe(subscriber: (arg:T) => void): void{
        this._subscribers.push(subscriber);
    }

    public UnSubscribe(subscriber: (arg:T) => void): void{
        let index = this._subscribers.indexOf(subscriber, 0);
        if (index > -1) {
            this._subscribers.splice(index, 1);
        }
    }

}

export default Action;