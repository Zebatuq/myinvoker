let mix = require('laravel-mix');
let ClosureCompilerPlugin = require('webpack-closure-compiler');

if (mix.inProduction()) {
    mix.webpackConfig({
        plugins: [
            new ClosureCompilerPlugin({
                compiler: {
                    language_in: 'ECMASCRIPT6',
                    language_out: 'ECMASCRIPT6',
                    compilation_level: 'ADVANCED',
                },
                concurrency: 3,
            })
        ],
    });
}

mix.disableNotifications();

mix.setPublicPath('../')
    .ts('src/*.ts', 'dist/MyInvoker.js');
